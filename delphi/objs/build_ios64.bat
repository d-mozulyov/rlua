@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "ios64\" mkdir ios64
cd ios64
set flags=-target x86_64-apple-darwin -c -O3

::compilation
clang %flags% %files%

cd ..


