@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "android32\" mkdir android32
cd android32
set flags=-target armv7-a-linux-android -march=armv7-a -mthumb -c -O3

::compilation
clang %flags% %files%

cd ..

