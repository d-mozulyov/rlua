@echo off
setlocal enabledelayedexpansion
set extra_path=..\..\..\src
for /f "tokens=*" %%x in (files.txt) do set sources=%%x
set files= 
for %%a in (%sources%) do set files=!files! %extra_path%\%%a
if not exist "win64\" mkdir win64
cd win64
set flags=-target x86_64-windows -c -O3 

::compilation
clang %flags% %files%

cd ..
pause



