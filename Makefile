CORE_C = lapi.c lcode.c lctype.c ldebug.c ldo.c ldump.c lfunc.c lgc.c llex.c \
	lmem.c lobject.c lopcodes.c lparser.c lstate.c lstring.c ltable.c \
	ltm.c lundump.c lvm.c lzio.c

LIB_C =	lauxlib.c lbaselib.c lbitlib.c lcorolib.c ldblib.c liolib.c \
	lmathlib.c loslib.c lstrlib.c ltablib.c lutf8lib.c loadlib.c linit.c

CC = clang

all:
	@echo "target all"



delphi_obj_win32: 
	@echo off
	cd delphi\objs
	build_win32.bat
	cd ..
	cd ..

delphi_obj_win64: 
	@echo off
	cd delphi\objs
	build_win64.bat
	cd ..
	cd ..

delphi_obj_linux32: 
	@echo off
	cd delphi\objs
	build_linux32.bat
	cd ..
	cd ..

delphi_obj_linux64: 
	@echo off
	cd delphi\objs
	build_linux64.bat
	cd ..
	cd ..

delphi_obj_mac32: 
	@echo off
	cd delphi\objs
	build_mac32.bat
	cd ..
	cd ..

delphi_obj_mac64: 
	@echo off
	cd delphi\objs
	build_mac64.bat
	cd ..
	cd ..

delphi_obj_android32: 
	@echo off
	cd delphi\objs
	build_android32.bat
	cd ..
	cd ..

delphi_obj_android64: 
	@echo off
	cd delphi\objs
	build_android64.bat
	cd ..
	cd ..

delphi_obj_ios32: 
	@echo off
	cd delphi\objs
	build_ios32.bat
	cd ..
	cd ..

delphi_obj_ios64: 
	@echo off
	cd delphi\objs
	build_ios64.bat
	cd ..
	cd ..



release_dyn_win32:
	if not exist "dll\" mkdir dll
	cd src
	$(CC) -o lua_win32.dll -target i686-win32 -shared $(CORE_C)
	move lua_win32.dll ..\dll
	cd ..

release_dyn_win64:
	if not exist "dll\" mkdir dll
	cd src
	$(CC) -o lua_win64.dll -target x86_64-windows -shared $(CORE_C)
	move lua_win64.dll ..\dll
	cd ..


debug_unittest_win32:
	@echo Здесь будет unit tests for win32 

debug_unittest_win64:
	@echo Здесь будет unit tests for win64 

debug_dyntest_win32:
	@echo Здесь будет dyntest win32 

debug_dyntest_win64:
	@echo Здесь будет dyntest win64




