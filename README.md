# R(apid) Lua - лучшие практики Lua и нативных языков

Цели данного проекта:
* Юникод
* Поддержка разумной многопоточности
* Ситуативная сборка мусора
* Низкоуровневая связка с нативной стороной
* Оптимизация по памяти и производительности
* IDE
 
Некоторые статьи будут появляться на [вики](https://gitlab.com/d-mozulyov/rlua/wikis/pages)